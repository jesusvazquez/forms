package v1

// API ~
type API struct {
}

// NewAPI is the constructor of the API. In order to initialize properly the API
// struct, the constructor will receive all necessary services as parameters.
func NewAPI() *API {
	return &API{}
}
