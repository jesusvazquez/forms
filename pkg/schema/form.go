package schema

import (
	"gitlab.com/jesusvazquez/forms/pkg/model"

	"github.com/graphql-go/graphql"
)

var formType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Form",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"title": &graphql.Field{
				Type: graphql.String,
			},
			"active": &graphql.Field{
				Type: graphql.Boolean,
			},
			"created_at": &graphql.Field{
				Type: graphql.DateTime,
			},
			"modified_at": &graphql.Field{
				Type: graphql.DateTime,
			},
		},
	},
)

// QueryForms by id (implicit)
var queryForms = graphql.Field{
	Name:        "QueryForms",
	Description: "Query Forms",
	Type:        formType,
	Args: graphql.FieldConfigArgument{
		"id": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.Int),
		},
	},
	Resolve: func(p graphql.ResolveParams) (result interface{}, err error) {
		id := p.Args["id"].(int)
		return model.GetFormByID(id)
	},
}

var queryListForms = graphql.Field{
	Name:        "QueryListForms",
	Description: "Query List Forms",
	Type:        graphql.NewList(formType),
	Resolve: func(p graphql.ResolveParams) (result interface{}, err error) {
		return model.GetForms(), nil
	},
}

var createForm = graphql.Field{
	Name:        "CreateForm",
	Description: "Create Form",
	Type:        formType,
	Args: graphql.FieldConfigArgument{
		"title": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: func(p graphql.ResolveParams) (result interface{}, err error) {
		title, _ := p.Args["title"].(string)

		newForm, err := model.CreateForm(title)

		if err != nil {
			return nil, err
		}

		return newForm, nil
	},
}

var deleteForm = graphql.Field{
	Name:        "deleteForm",
	Description: "Delete Form",
	Type:        formType,
	Args: graphql.FieldConfigArgument{
		"id": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.Int),
		},
	},
	Resolve: func(p graphql.ResolveParams) (result interface{}, err error) {
		id := p.Args["id"].(int)
		return model.DeleteFormByID(id)
	},
}

var updateFormTitle = graphql.Field{
	Name:        "updateFormTitle",
	Description: "Update Form Title",
	Type:        formType,
	Args: graphql.FieldConfigArgument{
		"id": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.Int),
		},
		"title": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: func(p graphql.ResolveParams) (result interface{}, err error) {
		id := p.Args["id"].(int)
		title := p.Args["title"].(string)
		return model.UpdateFormTitle(id, title)
	},
}
