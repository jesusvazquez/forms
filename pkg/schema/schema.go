package schema

import (
	"github.com/graphql-go/graphql"
)

var rootQuery = graphql.NewObject(
	graphql.ObjectConfig{
		Name:        "RootQuery",
		Description: "Root Query",
		Fields: graphql.Fields{
			"forms":     &queryForms,
			"formsList": &queryListForms,
		},
	})

// No mutations yet
var rootMutation = graphql.NewObject(
	graphql.ObjectConfig{
		Name:        "RootMutation",
		Description: "Root Mutation",
		Fields: graphql.Fields{
			"createForm":      &createForm,
			"deleteForm":      &deleteForm,
			"updateFormTitle": &updateFormTitle,
		},
	})

// Schema is the GraphQL schema served by the server.
var Schema, _ = graphql.NewSchema(
	graphql.SchemaConfig{
		Query:    rootQuery,
		Mutation: rootMutation,
	})
