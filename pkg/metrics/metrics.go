package metrics

import "github.com/prometheus/client_golang/prometheus"

// Metrics struct holds the metrics of the service. With just a quick
// glance you may realize that we're trying to use the RED method.
type Metrics struct {
	GinRequestsTotal      *prometheus.CounterVec
	GinRequestErrorsTotal *prometheus.CounterVec
	GinRequestDuration    *prometheus.SummaryVec
}

// RegisterMetrics ~
func RegisterMetrics() Metrics {
	var m Metrics

	m.GinRequestsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "gin_requests_total",
			Help: "Total requests processed",
		}, []string{"code", "endpoint", "method"},
	)
	prometheus.MustRegister(m.GinRequestsTotal)

	m.GinRequestErrorsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "gin_request_errors_total",
			Help: "Total requests failed",
		}, []string{"code", "endpoint", "method"},
	)
	prometheus.MustRegister(m.GinRequestErrorsTotal)

	m.GinRequestDuration = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name:       "gin_request_duration_seconds",
			Help:       "Request latencies in seconds",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		}, []string{"endpoint"},
	)
	prometheus.MustRegister(m.GinRequestDuration)

	return m
}
