package model

import (
	"errors"
	"fmt"
)

// ErrFormNotExist represents a "Form does not exist" error
type ErrFormNotExist struct {
	FormID int
}

// IsErrFormNotExist checks if an error is ErrFormNotExist
func IsErrFormNotExist(err error) bool {
	return errors.As(err, &ErrFormNotExist{})
}

func (err ErrFormNotExist) Error() string {
	return fmt.Sprintf("form does not exist [id: %d]", err.FormID)
}

// ErrFormHasNoID represents a "This form has no id" kind of error.
type ErrFormHasNoID struct{}

// IsErrFormHasNoID checks if an error is a ErrFormHasNoID.
func IsErrFormHasNoID(err error) bool {
	return errors.As(err, &ErrFormHasNoID{})
}

func (err ErrFormHasNoID) Error() string {
	return fmt.Sprintf("form has no ID, can't operate with it")
}

// ErrFormCantDelete represents a "Can't delete Form" error
type ErrCantDeleteForm struct {
	FormID int
}

// IsErrCantDeleteForm checks if an error is ErrCantDeleteForm
func IsErrCantDeleteForm(err error) bool {
	return errors.As(err, &ErrCantDeleteForm{})
}

func (err ErrCantDeleteForm) Error() string {
	return fmt.Sprintf("can't delete form [id: %d]", err.FormID)
}
