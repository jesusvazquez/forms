package model

import (
	"time"
)

// Form ~
type Form struct {
	FormID     int       `json:"id"`
	Title      string    `json:"title"`
	Active     bool      `json:"active"`
	CreatedAt  time.Time `json:"created_at"`
	ModifiedAt time.Time `json:"modified_at"`
}

// NewForm ~
func NewForm(title string) *Form {
	return &Form{
		Title:      title,
		Active:     true,
		CreatedAt:  time.Now(),
		ModifiedAt: time.Now(),
	}
}

var forms = []Form{
	{
		FormID:     1,
		Title:      "Questionnaire about our product",
		Active:     true,
		CreatedAt:  time.Now(),
		ModifiedAt: time.Now(),
	},
	{
		FormID:     2,
		Title:      "Questionnaire about our personalized experience",
		Active:     true,
		CreatedAt:  time.Now(),
		ModifiedAt: time.Now(),
	},
}

func CreateForm(title string) (*Form, error) {
	form := NewForm(title)
	form.FormID = len(forms) + 1
	forms = append(forms, *form)
	return form, nil
}

// func createForm(somedatabaseclient, id) (*Form, error) {}

func GetFormByID(id int) (*Form, error) {
	return getFormbyID(id)
}

func getFormbyID(id int) (*Form, error) {
	// TODO reimplement using DB
	for _, form := range forms {
		if form.FormID == id {
			return &form, nil
		}
	}
	return nil, ErrFormNotExist{id}
}

func GetForms() []Form {
	return forms
}

// func getForm(somedatabaseclient, id) (*[]Form, error) {}

func DeleteFormByID(formID int) (*Form, error) {
	return deleteFormByID(formID)
}

func deleteFormByID(formID int) (*Form, error) {
	// TODO reimplement this with DB
	form, err := getFormbyID(formID)
	if err != nil {
		return nil, err
	}
	form.Active = false
	forms[formID-1] = *form
	return form, nil
}

func UpdateFormTitle(id int, title string) (*Form, error) {
	return updateFormTitle(id, title)
}

func updateFormTitle(id int, title string) (*Form, error) {
	form, err := getFormbyID(id)
	if err != nil {
		return nil, err
	}
	form.Title = title
	forms[form.FormID-1] = *form
	return form, nil
}

// exists checks if given form actually exists
func exists(form *Form) (bool, error) {
	// TODO reimplmenet this with DB
	if form.FormID < len(forms) {
		if form.FormID == forms[form.FormID-1].FormID {
			return true, nil
		}
	}
	return false, nil
}

// isEmpty checks if form is a new form has been initialized
func isEmpty(form *Form) (bool, error) {
	if (form == &Form{}) || (form.FormID == 0) {
		return false, ErrFormHasNoID{}
	}
	return true, nil
}
