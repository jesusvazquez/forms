package model

import (
	"database/sql"
	"fmt"
)

var (
	db *sql.DB
)

// getEngine initializes the db client and returns it
func getEngine() (db *sql.DB, err error) {
	// TODO make this variables come from global config
	port := 3306
	user := "test"
	password := "test"
	host := "127.0.0.1"
	dbname := "test"

	// Define datasource with all the params
	dataSourceName := fmt.Sprintf("%s:%s@(%s:%d)/%s?parseTime=true", user,
		password, host, port, dbname)
	// Initialize sql object
	db, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		return nil, err
	}
	return db, err
}

// SetEngine Sets the DB client
func SetEngine() (err error) {
	db, err = getEngine()
	if err != nil {
		return err
	}
	return nil
}

// NewEngine initializes the database engine of the service
func NewEngine() (err error) {
	err = SetEngine()
	if err != nil {
		return err
	}

	// TODO ping database, make sure to return error if fail.
	return nil
}
