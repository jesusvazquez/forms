package service

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/jesusvazquez/forms/pkg/metrics"

	"github.com/sirupsen/logrus"
)

// Service definition
type Service struct {
	Server  *http.Server
	Metrics metrics.Metrics
}

// Specification parameters for the service
type Specification struct {
	Host string `default:"0.0.0.0"`
	Port string `default:"3000"`
}

// Start the service
func (s *Service) Start() {
	// Run http server in a goroutine
	go func() {
		if err := s.Server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logrus.Fatalf("listen: %s\n", err)
		}
	}()
	logrus.Info("Server listening in " + s.Server.Addr)
}

// WaitForSignal blocks for a termination signal to stop
func (s *Service) WaitForSignal() {
	// Initialize graceful shutdown
	var gracefulShutdown = make(chan os.Signal)
	signal.Notify(gracefulShutdown, syscall.SIGTERM, syscall.SIGINT,
		os.Interrupt)
	logrus.Info("Waiting for stop signal...")

	// Wait for signal
	sig := <-gracefulShutdown
	logrus.Printf("Received signal: %v. Stopping...", sig)

	// Stop http server
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := s.Server.Shutdown(ctx); err != nil {
		logrus.Fatal("Server Shutdown:", err)
	}
	os.Exit(0)
}
