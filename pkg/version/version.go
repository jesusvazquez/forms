package version

import "fmt"

// Version represents the current version of the service.
type Version struct {
	Major int
	Minor int
	Patch int
}

func (v Version) String() string {
	if v.Major == 0 && v.Minor == 0 && v.Patch == 0 {
		return "Development version"
	}
	return fmt.Sprintf("v%d.%d.%d", v.Major, v.Minor, v.Patch)
}
