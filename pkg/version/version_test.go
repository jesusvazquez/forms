package version_test

import (
	"testing"

	"gitlab.com/jesusvazquez/forms/pkg/version"
)

var versionTable = []struct {
	version  version.Version
	expected string
}{
	{
		version.Version{},
		"Development version",
	},
	{
		version.Version{
			Major: 1,
		},
		"v1.0.0",
	},
	{
		version.Version{
			Minor: 1,
		},
		"v0.1.0",
	},
	{
		version.Version{
			Patch: 1,
		},
		"v0.0.1",
	},
	{
		version.Version{
			Minor: 1,
			Patch: 1,
		},
		"v0.1.1",
	},
	{
		version.Version{
			Major: 1,
			Minor: 1,
			Patch: 1,
		},
		"v1.1.1",
	},
}

func TestString(t *testing.T) {
	for _, e := range versionTable {
		t.Run(e.expected, func(t *testing.T) {
			if e.version.String() != e.expected {
				t.Errorf("Expected version %s does not match result %s", e.expected,
					e.version.String())
			}
		})
	}
}
