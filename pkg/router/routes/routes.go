package routes

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	ginlogrus "github.com/toorop/gin-logrus"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	api "gitlab.com/jesusvazquez/forms/pkg/api/v1"
	"gitlab.com/jesusvazquez/forms/pkg/metrics"
	"gitlab.com/jesusvazquez/forms/pkg/router"
)

// NewRouter builds the routes for the service and returns a gin engine
func NewRouter(api *api.API, m *metrics.Metrics) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	e := gin.New()

	// Instantiate middlewares for logging and metrics
	e.Use(ginlogrus.Logger(logrus.New()), gin.Recovery())
	e.Use(InstrumentHandlerFunc(m))

	// Default endpoints
	e.GET("/status", status)
	e.GET("/metrics", router.Metrics())

	g := e.Group("/graphql")
	{
		g.POST("", router.GraphQL())
		g.GET("", router.GraphQL())
	}

	return e
}

// InstrumentHandlerFunc wrapper to instrument gin functions. The purpose is to
// get prometheus instrumentation using the RED method.
func InstrumentHandlerFunc(m *metrics.Metrics) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Measure duration
		timer := prometheus.NewTimer(
			m.GinRequestDuration.WithLabelValues(c.Request.RequestURI))
		defer timer.ObserveDuration()

		c.Next()

		// Increase global counter
		m.GinRequestsTotal.WithLabelValues(
			strconv.Itoa(c.Writer.Status()),
			c.Request.RequestURI,
			strings.ToLower(c.Request.Method)).Inc()

		// If there is an error increase global error counter
		if c.Writer.Status() >= 400 {
			m.GinRequestErrorsTotal.WithLabelValues(
				strconv.Itoa(c.Writer.Status()),
				c.Request.RequestURI,
				strings.ToLower(c.Request.Method)).Inc()
		}
	}
}

// status endpoint to expose service livenessprobe
func status(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, gin.H{
		"status": "ok",
	})
}
