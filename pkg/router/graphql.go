package router

import (
	"github.com/gin-gonic/gin"
	"github.com/graphql-go/handler"
	"gitlab.com/jesusvazquez/forms/pkg/schema"
)

// GraphQL initializes the graphql handler.
func GraphQL() gin.HandlerFunc {
	// Creates a GraphQL-go HTTP handler with the defined schema
	h := handler.New(&handler.Config{
		Schema:   &schema.Schema,
		Pretty:   false,
		GraphiQL: true,
	})

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
