package router

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Metrics initializes the prometheus handler.
func Metrics() gin.HandlerFunc {
	h := promhttp.Handler()

	return func(c *gin.Context) {
		// TODO Evaluate if we need to set up authentication here to avoid
		// exposing metrics
		h.ServeHTTP(c.Writer, c.Request)
	}
}
