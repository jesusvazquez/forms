#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Simple bash script to run the application with env variables
set -eufo pipefail
export SHELLOPTS    # propagate set to children by default
IFS=$'\t\n'

export BACKEND_HOST="0.0.0.0"
export BACKEND_PORT="3000"

go run cmd/app/main.go
