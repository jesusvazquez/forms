#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Check script that will run most relevant go tools over the code.
set -eufo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

umask 0077	# by default create only owner-readable files

# Run golint
golint -set_exit_status ./..
echo "# golint OK"

# Iterate over all project files except for vendor dependencies and run gofmt
# and goimports
while IFS= read -r -d $'\0' gofile
do
    FILE_TO_FIX_IMPORTS=$(goimports -l "${gofile}" | wc -l)
    if [ ! "${FILE_TO_FIX_IMPORTS}" -eq 0 ]
    then
        echo "fix your imports, try running goimports -w"
    fi
    FILE_TO_FIX_FORMAT=$(gofmt -l "${gofile}" | wc -l)
    if [ ! "${FILE_TO_FIX_FORMAT}" -eq 0 ]
    then
        echo "fix your formatting, try running gofmt -w"
    fi
done < <(find . -type f -name '*.go' -not -path "./vendor/*" -print0)
echo "# gofmt OK"
echo "# goimports OK"