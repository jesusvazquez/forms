#!/bin/bash
curl \
  --silent \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{ "query": "mutation { createForm(title: \"Would you recommend it to your colleagues?\") { id,title } }" }' \
  http://localhost:3000/graphql \
  | jq .