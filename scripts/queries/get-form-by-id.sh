#!/bin/bash
# Get would be
# curl -g 'localhost:3000/graphql?query={forms(id:1){id,title}}' | jq .
curl \
  --silent \
  -X POST \
  -H "Content-Type: application/json" \
  -d "{ \"query\": \"{ forms(id:$1) { id title }}\" }" \
  http://localhost:3000/graphql \
  | jq .