package main

import (
	"net/http"

	_ "github.com/go-sql-driver/mysql"

	"github.com/kelseyhightower/envconfig"
	api "gitlab.com/jesusvazquez/forms/pkg/api/v1"
	"gitlab.com/jesusvazquez/forms/pkg/metrics"
	"gitlab.com/jesusvazquez/forms/pkg/router/routes"
	service "gitlab.com/jesusvazquez/forms/pkg/service"

	"github.com/sirupsen/logrus"
)

func main() {

	// Load environment variables or fail horribly
	var spec service.Specification
	err := envconfig.Process("BACKEND", &spec)
	if err != nil {
		logrus.Fatal(err.Error())
	}

	// Initialize and register service metrics
	metrics := metrics.RegisterMetrics()

	// Initialize the API with the previously loaded set of services.
	api := api.NewAPI()

	// Initialize application router
	r := routes.NewRouter(api, &metrics)

	// Initialize http server
	server := &http.Server{
		Addr:    spec.Host + ":" + spec.Port,
		Handler: r,
	}

	// Define application service global object
	s := service.Service{
		Metrics: metrics,
		Server:  server,
	}

	// Begin service execution
	s.Start()

	// Waits for an interruption signal to graceful shutdown all components
	s.WaitForSignal()
}
