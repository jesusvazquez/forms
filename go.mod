module gitlab.com/jesusvazquez/forms

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/graphql-go/graphql v0.7.8
	github.com/graphql-go/handler v0.2.3
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/prometheus/client_golang v1.3.0
	github.com/sirupsen/logrus v1.4.2
	github.com/toorop/gin-logrus v0.0.0-20190701131413-6c374ad36b67
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
