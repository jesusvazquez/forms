# Use Golang image as Base image
FROM golang:1.13 AS builder
WORKDIR $GOPATH/src/gitlab.com/jesusvazquez/forms
COPY . ./
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /app/forms cmd/app/main.go

# Build final smallest image
FROM scratch
COPY --from=builder /app/forms /app/backend
WORKDIR /app
ENTRYPOINT ["/app/forms"]