# Forms

Example Golang backend with a GraphQL API.

## TODO List

- [x] Initial structure
- [x] GraphQL endpoint.
  - [x] pkg/schema with root entrypoint and subtypes
- [x] Prometheus metrics
- [x] Form CRUD
  - [x] Get by ID, Get List
  - [x] Create
  - [x] Delete
  - [x] Update
- [x] Dockerize
- [ ] Database
  - [x] Initial structure for db engine
  - [ ] Define Form schema
  - [ ] Initialize client DB on start
  - [ ] Update pkg/models
    - [ ] models.go have a reference to the db client
    - [ ] form.go instance private methods to get forms from DB instead of hardcoded variable.

## Development

We're using `go mod` to manage dependencies. Run `go mod download` to start working.

After that you can start the backend with:

```
❯ go run cmd/app/main.go
INFO[0000] Server listening in 0.0.0.0:8080
INFO[0000] Waiting for stop signal...
```

## Run some example queries

Note: It's recommended to have jq installed

- Get by ID

```bash
❯ curl \
  --silent \
  -X POST \
  -H "Content-Type: application/json" \
  -d "{ \"query\": \"{ forms(id:1) { id title }}\" }" \
  http://localhost:3000/graphql \
  | jq .

{
  "data": {
    "forms": {
      "id": 1,
      "title": "Questionnaire about our product"
    }
  }
}
```

- Get List

```bash
❯ curl \
  --silent \
  -g \
  'localhost:3000/graphql?query={formsList{id,title}}' \
  | jq .

{
  "data": {
    "formsList": [
      {
        "id": 1,
        "title": "Questionnaire about our product"
      },
      {
        "id": 2,
        "title": "Questionnaire about our personalized experience"
      }
    ]
  }
}
```

- Create form

```bash
❯ curl \
  --silent \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{ "query": "mutation { createForm(title: \"Would you recommend it to your colleagues?\") { id,title } }" }' \
  http://localhost:3000/graphql \
  | jq .

{
  "data": {
    "createForm": {
      "id": 3,
      "title": "Would you recommend it to your colleagues?"
    }
  }
}
```

- Delete form

```bash
❯ curl \
  --silent \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{ "query": "mutation { deleteForm(id:1) { id,title,active } }" }' \
  http://localhost:3000/graphql \
  | jq .

{
  "data": {
    "deleteForm": {
      "active": false,
      "id": 1,
      "title": "Questionnaire about our product"
    }
  }
}
```

- Update form

```bash
❯ curl \
  --silent \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{ "query": "mutation { updateFormTitle(id:1,title: \"My new title\") { id,title created_at, modified_at} }" }' \
  http://localhost:3000/graphql \
  | jq .

{
  "data": {
    "updateFormTitle": {
      "created_at": "2020-01-27T22:27:33.267172969+01:00",
      "id": 1,
      "modified_at": "2020-01-27T22:27:33.267173042+01:00",
      "title": "My new title"
    }
  }
}
```
